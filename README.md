
# Coding Exercise Instructions #

* The goal of this exercise is to get an idea of the code you would produce if you were working at GoSweat.

## Your submission ##

* should be production quality: this is as important as solving the problem itself. 
* should be created using Java or Javascript 
* should be executable from the command line
* should provide a class for starting the application but you don't need to package the application. 
* should include unit tests.
* could provide a README, e.g. to clarify any assumption you have made. 

We want to make it easy for you to submit, and for us to review your submission so please follow these steps:
* Sign up for a free personal account on http://bitbucket.org.
* Create a private fork of https://bitbucket.org/gosweatbootcamp/coding-exercise.git   
* Commit and push your submission to your fork.
* Share your forked repository with the user gosweat_bootcamp
* We'll acknowledge receipt.